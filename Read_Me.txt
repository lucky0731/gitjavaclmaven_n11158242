Documentation for Monte Carlo simulation with Git, JAVACL and Maven

Author: Wenjun Song
Student ID: N11158242

This project is a Maven project. You may run the “SimulationManager.java” to test the project.

The configuration for the Maven project is in “pom.xml” file.

The Result of simulation is as following:
	1. European Call Option:
		European Call Option price: euroCallOptionPrice=6.2192800569759825
		Estimated standard deviation: sigma_hat=12.98570429863626
		Number of simulations : N=7121225
	2. Asian Call Option:
		Asian Call Option price: asianCallOptionPrice=2.191612971208483
		Estimated standard deviation: sigma_hat=5.340001728096479
		Number of simulations : N=1204222

There're 10 classes in the src/main/java/montecarlo directory, inside which only the “SimulationManager.java” and “GPUNormalRandomVectorGenerator.java” are changed for using GPU, all the others stay the same as the former Monte Carlo Simulation project:
		1.SimulationManager.java -- This contains the main function for the project.
		2.AntiTheticVectorGenerator.java
		3.AsianCallOption.java
		4.EuropeanCallOption.java
		5.GBMRandomPathGenerator.java
		6.GPUNormalRandomVectorGenerator.java
		7.PayOut.java
		8.RandomVectorGenerator.java
		9.StatsCaculator.java
		10.StockPath.java

And there're 6 classes in the src/test/java/montecarlo directory corresponding to the 6 classes in src/main/java/montecarlo directory:
		1.Test_AntiTheticVectorGenerator.java
		2.Test_AsianCallOption.java
		3.Test_EuropeanCallOption.java
		4.Test_GBMRandomPathGenerator.java
		5.Test_GPUNormalRandomVectorGenerator.java
		6.Test_StatsCaculator.java

There’re also 8 test example in the /src/main/java/example directory including helloworld.java and professor’s examples:
		1. helloworld.java
		2. Example1.java
		3. Example2.java
		4. Example3.java
		5. Example4.java
		6. Example5.java
		7. Example6.java
		8. Example7.java


