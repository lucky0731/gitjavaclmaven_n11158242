package montecarlo;

import java.util.List;

import org.apache.commons.math3.util.Pair;
import org.joda.time.DateTime;
/*
 * @auther Wenjun Song
 * ID#: N11158242
 * This class takes an stock path and returns an Asian Call Option pay out back
 * */
public class AsianCallOption implements PayOut {

	private double K;
	
	public AsianCallOption(double K){
		this.K=K;
	}
	
	@Override
	public double getPayout(StockPath path) {
		double sumPrice=0;
		List<Pair<DateTime,Double>> prices=path.getPrices();
		for(Pair<DateTime,Double> aPair:prices)
			sumPrice+=aPair.getSecond().doubleValue();
		sumPrice/=prices.size();
		return Math.max(0, sumPrice-K);
	}

}
