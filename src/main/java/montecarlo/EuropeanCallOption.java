package montecarlo;

import org.apache.commons.math3.util.Pair;
import org.joda.time.DateTime;
/*
 * @auther Wenjun Song
 * ID#: N11158242
 * This class takes an stock path and returns an European Call Option pay out back
 * */
public class EuropeanCallOption implements PayOut {

	private double K;
	
	public EuropeanCallOption(double K){
		this.K=K;
	}
	
	@Override
	public double getPayout(StockPath path) {
		Pair<DateTime,Double> lastPair=path.getPrices().get(path.getPrices().size()-1);
		return Math.max(0, lastPair.getSecond().doubleValue()-K);
	}

}
