package montecarlo;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.math3.util.Pair;
import org.joda.time.DateTime;
/*
 * @auther Wenjun Song
 * ID#: N11158242
 * This class generate the stock prices path based on the geometric brownian motion
 * */
 
public class GBMRandomPathGenerator implements StockPath {

	private double rate;
	private double sigma;
	private double S0;
	private int N;
	private DateTime startDate;
	private DateTime endDate;
	private RandomVectorGenerator rvg;
	
	//Constructor
	public GBMRandomPathGenerator(double rate,int N,double sigma,double S0,DateTime startDate,DateTime endDate,RandomVectorGenerator rvg){
		this.rate=rate;
		this.sigma=sigma;
		this.S0=S0;
		this.N=N;
		this.startDate=startDate;
		this.endDate=endDate;
		this.rvg=rvg;
	}
	
	@Override
	public List<Pair<DateTime, Double>> getPrices() {
		double[] n=rvg.getVector();
		DateTime current=new DateTime(startDate.getMillis());
		long delta=(endDate.getMillis()-startDate.getMillis())/N;
		List<Pair<DateTime,Double>> path=new LinkedList<Pair<DateTime,Double>>();
		path.add(new Pair<DateTime,Double>(current,S0));
		for(int i=1;i<N+1;i++){
			current=current.plusMillis((int) delta);
			path.add(new Pair<DateTime,Double>(current,path.get(path.size()-1).getValue()*Math.exp((rate-sigma*sigma/2)+sigma*n[i-1])));
		}
		return path;
	}

}
