package montecarlo;

import java.util.Random;
import com.nativelibs4java.opencl.*;
import org.bridj.Pointer;
import static java.lang.Math.PI;
import java.util.Arrays;

import static org.bridj.Pointer.allocateFloats;
/*
 * @auther Wenjun Song
 * ID#: N11158242
 * This class implement a Normal Random Vector Generator which generate a random vector follows
 * standard normal distribution with a given length n on GPU;
 * */
public class GPUNormalRandomVectorGenerator implements RandomVectorGenerator  {

    //define length of each random vector that's gonna be returned
    private int len;
    //define the number of random number generated in one batch
    private int batch;
    //define the array contains the random one batch of random number
    protected double[] bigNormalRandomVector;
    //define the index of the next available random number
    private int nextAvailableElementIndex;

    //Constructor taking a parameter of each vector length n and a batch number to generate random vector,
    //and generate a initial batch of random numbers
	public GPUNormalRandomVectorGenerator(int n,int batch){
        len=n;
        // make sure batch number is even number
        int temp=batch/2;
		this.batch=temp*2;
        //generate a first batch of random numbers
		generateOneBatchRandomVector();
	}

    // return a random vector of length n, which follows a standard normal distribution,
    // and generate a new batch of random numbers if necessary
    @Override
    public double[] getVector(){
        //check if there's enough random number left, if not generate a new batch
        if(nextAvailableElementIndex+len>batch){
            generateOneBatchRandomVector();
            nextAvailableElementIndex=0;
        }
        //copy the next slice of random number to a small array
        double[] randomVector=Arrays.copyOfRange(bigNormalRandomVector,nextAvailableElementIndex,nextAvailableElementIndex+len);
        //set the start index to the next slice
        nextAvailableElementIndex=nextAvailableElementIndex+len;
        return randomVector;
    }

    //this function generates a batch of random numbers
	public void generateOneBatchRandomVector(){
        bigNormalRandomVector=new double[this.batch];
        nextAvailableElementIndex=0;
        // Creating the platform which is out computer.
        CLPlatform clPlatform = JavaCL.listPlatforms()[0];
        // Getting the GPU device
        CLDevice device = clPlatform.getBestDevice();
        // Let's make a context
        CLContext context = JavaCL.createContext(null, device);
        // Lets make a default FIFO queue.
        CLQueue queue = context.createDefaultQueue();

        // Read the program sources and compile them :
        String src =
                "__kernel void normal_random_number(__global const float* x1, __global const float* x2, __global float* y1, __global float* y2, float pi,int n) \n" +
                "{\n" +
                "    int i = get_global_id(0);\n" +
                "    if (i >= n)\n" +
                "        return;\n" +
                "\n" +
                "    y1[i] = sqrt(-2*log(x1[i]))*cos(2*pi*x2[i]);\n" +
                "    y2[i] = sqrt(-2*log(x1[i]))*sin(2*pi*x2[i]);\n" +
                "}";
        CLProgram program = context.createProgram(src);
        program.build();
        //create kernel
        CLKernel kernel = program.createKernel("normal_random_number");

        //set the length of input to half of batch size, because each pair of of input can generate two random numbers
        final int n=batch/2;

        //Allocate memory size for the two input pointers
        final Pointer<Float>
                x1Ptr = allocateFloats(n),
                x2Ptr = allocateFloats(n);
        //generate the initial input vectors, which are two n length uniform random array.
        Random uniformNumberGenerator=new Random();
        for (int i=0;i<n;i++){
            x1Ptr.set(i,(float)uniformNumberGenerator.nextDouble());
            x2Ptr.set(i,(float)uniformNumberGenerator.nextDouble());
        }

        // Create OpenCL input buffers (using the native memory pointers aPtr and bPtr) :
        CLBuffer<Float>
                x1 = context.createFloatBuffer(CLMem.Usage.Input, x1Ptr),
                x2 = context.createFloatBuffer(CLMem.Usage.Input, x2Ptr);

        // Create an OpenCL output buffer :
        CLBuffer<Float> y1 = context.createFloatBuffer(CLMem.Usage.Output,n);
        CLBuffer<Float> y2 = context.createFloatBuffer(CLMem.Usage.Output,n);
        kernel.setArgs(x1,x2,y1,y2,(float)PI, n);

        CLEvent event = kernel.enqueueNDRange(queue, new int[]{n}, new int[]{128});
        //Allocate memory size for the two output pointers
        final Pointer<Float> y1Ptr = y1.read(queue,event);
        final Pointer<Float> y2Ptr = y2.read(queue,event);
        //put the generated random number to our big array
        for(int i =0;i<n;i++){
            bigNormalRandomVector[i*2] = (double)y1Ptr.get(i);
            bigNormalRandomVector[i*2+1] = (double)y2Ptr.get(i);
        }
    }
}
