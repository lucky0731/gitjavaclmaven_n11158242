package montecarlo;

/*
 * @auther Wenjun Song
 * ID#: N11158242
 * This interface takes an stock path and returns an pay out back
 * */

public interface PayOut {

	public double getPayout(StockPath path);
	
}
