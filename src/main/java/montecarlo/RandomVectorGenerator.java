package montecarlo;

/*
 * @auther Wenjun Song
 * ID#: N11158242
 * This interface defines a method which return a random vector
 * */

public interface RandomVectorGenerator {
	
	public double[] getVector();

}
