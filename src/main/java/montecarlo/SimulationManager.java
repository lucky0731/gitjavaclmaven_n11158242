package montecarlo;

import java.util.List;

import org.apache.commons.math3.util.Pair;
import org.joda.time.DateTime;

/*
 * @auther Wenjun Song
 * ID#: N11158242
 * This class contains the main function of our simulation, which takes all the input and does the simulation
 * */

public class SimulationManager {
	
	//the main function for the project
	
	public static void main(String[] args){
		//y=2.055 such that P(|Y|<y)=96% for Y~N(0,1)
		double y=2.055;
		//define the err level
		double err=0.01;
		//set a initial simulation number to estimate the standard deviation
		int N=1000;
		//define the size of batch to generate random number from GPU
		int batch=512*512*8;
		//define interest
		double rate=0.0001;
		//define the standard deviation
		double sigma=0.01;
		//define the initial underlying price
		double S0=152.35;
		//define the time period
		int days=252;
		//define the European Call Option strike price
		double K=165;
		//define the start date
		DateTime startDate=new DateTime();
		//define the end date
		DateTime endDate=startDate.plusDays(days);
		//initialize the GPU version normal random generator
		RandomVectorGenerator rvg=new GPUNormalRandomVectorGenerator(days,batch);
		RandomVectorGenerator arvg=new AntiTheticVectorGenerator(rvg);

		// run the simulation of European Call Option
		double euroCallOptionPrice=doEuropeanCallOptionSimulation(y,N,err,K,rate,days,sigma,S0,startDate,endDate,arvg);
		// print the results
		System.out.print("European Call Option Price : ");
		System.out.println(euroCallOptionPrice);

		//set the Asian Call Option strike price
		K=164;
		// reinitialize the  GPU version normal random generator
		rvg=new GPUNormalRandomVectorGenerator(days,batch);
		arvg=new AntiTheticVectorGenerator(rvg);
		// run the simulation of Asian Call Option
		double asianCallOptionPrice=doAsianCallOptionSimulation(y,N,err,K,rate,days,sigma,S0,startDate,endDate,arvg);
		// print the results
		System.out.print("Asian Call Option Price : ");
		System.out.println(asianCallOptionPrice);
	}
	
	//this method does the European Call Option simulation
	public static double doEuropeanCallOptionSimulation(double y,int N,double err,double K,double rate,int days,double sigma,double S0,DateTime startDate,DateTime endDate,RandomVectorGenerator rvg){
		//define the payout to be European type
		PayOut euroPayOut=new EuropeanCallOption(K);
		// run the European Call Option simulation and return the price
		return doSimulation(y,N,err,K,rate,days,sigma,S0,startDate,endDate,rvg,euroPayOut);
	}
	
	//this method does the Asian Call Option simulation
	public static double doAsianCallOptionSimulation(double y,int N,double err,double K,double rate,int days,double sigma,double S0,DateTime startDate,DateTime endDate,RandomVectorGenerator rvg){
		//define the payout to be Asian type
		PayOut asianPayOut=new AsianCallOption(K);
		// run the Asian Call Option simulation and return the price
		return doSimulation(y,N,err,K,rate,days,sigma,S0,startDate,endDate,rvg,asianPayOut);
	}
	
	//this method does the Monte Carlo simulation and return the price according to given pay out
	public static double doSimulation(double y,int N,double err,double K,double rate,int days,double sigma,double S0,DateTime startDate,DateTime endDate,RandomVectorGenerator rvg,PayOut payout){
		//generate stock path from geometric brownian motion
		StockPath gbm=new GBMRandomPathGenerator(rate,days,sigma,S0,startDate,endDate,rvg);
		//initialize the statistics calculator to track the statistics of the simulation
		StatsCaculator statsCaculator=new StatsCaculator();
		//try running the simulation N times
		for(int i=0;i<N;i++){
			//get the option price for one path of simulation
			double price=payout.getPayout(gbm);
			//update the statistics, mean and standard deviation
			statsCaculator.calculateNewStandardDeviation(price);
		}

		//according to the standard deviation from the trying simulation to estimate the number of simulations we need to
		//run in order to get certain accuracy
		N=(int)Math.ceil(Math.pow(y*statsCaculator.getStandardDeviation()/err,2));
		//print out the standard deviation of the simulation and the simulation number we need to run
		System.out.print("Sigma : ");
		System.out.println(statsCaculator.getStandardDeviation());
		System.out.print("Number of Simulation : ");
		System.out.println(N);
		//according to our estimation of simulation path needed, run the simulation officially
		for(int i=0;i<N;i++){
			//get the option price for one path of simulation
			double price=payout.getPayout(gbm);
			//update the statistics, mean and standard deviation
			statsCaculator.calculateNewStandardDeviation(price);
		}
		//return the price result of the simulation
		return statsCaculator.getRunningAverage()*Math.exp(-rate*days);
	}
	
}
