package montecarlo;

/*
 * @auther Wenjun Song
 * ID#: N11158242
 * This class takes the Stock price path and calculate the standard deviation and mean
 * */
public class StatsCaculator {

	private double mu;
	private double xSqrt;
	private int counter;
	
	//Constructor
	public StatsCaculator(){
		mu=0;
		xSqrt=0;
		counter=0;
	}
	
	public double calculateNewStandardDeviation(double newPrice){
		mu=(counter*mu+newPrice)/(counter+1);
		xSqrt=(counter*xSqrt+newPrice*newPrice)/(counter+1);
		counter+=1;
		return Math.sqrt(xSqrt-mu*mu);
	}
	
	public double getStandardDeviation(){
		return Math.sqrt(xSqrt-mu*mu);
	}
	
	public double getRunningAverage(){
		return mu;
	}
	public void print(){
		System.out.println("counter: "+counter);
		System.out.println("mu: "+mu);
		System.out.println("xSqrt: "+xSqrt);
	}
}
