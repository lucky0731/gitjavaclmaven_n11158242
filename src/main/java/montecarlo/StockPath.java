package montecarlo;

import java.util.List;

import org.apache.commons.math3.util.Pair;
import org.joda.time.DateTime;
/*
 * @auther Wenjun Song
 * ID#: N11158242
 * This is an interface that returns an date-price pair list back
 * */

public interface StockPath {
	public List<Pair<DateTime,Double>> getPrices();
}
