package montecarlo;

import junit.framework.TestCase;

public class Test_AntiTheticVectorGenerator extends TestCase {

	//Test for the constructor
	public void testAntiTheticVectorGenerator() {
		RandomVectorGenerator rvg=new GPUNormalRandomVectorGenerator(4,1024);
		AntiTheticVectorGenerator avg=new AntiTheticVectorGenerator(rvg);		
	}

	//Test for the getVector method
	public void testGetVector() {
		RandomVectorGenerator rvg=new GPUNormalRandomVectorGenerator(4,1024);
		AntiTheticVectorGenerator avg=new AntiTheticVectorGenerator(rvg);
		
		double[] firstVector=avg.getVector();
		double[] secondVector=avg.getVector();
		double[] thirdVector=avg.getVector();
		assertTrue(firstVector[0]==-secondVector[0]&&firstVector[1]==-secondVector[1]&&firstVector[2]==-secondVector[2]&&firstVector[3]==-secondVector[3]);
		assertTrue(firstVector[0]!=thirdVector[0]||firstVector[1]!=thirdVector[1]||firstVector[2]!=thirdVector[2]||firstVector[3]!=thirdVector[3]);
	}

}
