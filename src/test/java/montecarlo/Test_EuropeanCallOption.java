package montecarlo;

import org.joda.time.DateTime;

import junit.framework.TestCase;

public class Test_EuropeanCallOption extends TestCase {

	public void testEuropeanCallOption() {
		PayOut payOut=new EuropeanCallOption(1);
	}

	public void testGetPayout() {
		double rate=0.0001;
		double sigma=0.01;
		double S0=152.35;
		int days=2;
		DateTime startDate=new DateTime();
		DateTime endDate=startDate.plusDays(days);
		RandomVectorGenerator rvg=new GPUNormalRandomVectorGenerator(days,1024);
		StockPath gbm=new GBMRandomPathGenerator(rate,days,sigma,S0,startDate,endDate,rvg);
		double K=165;
		PayOut euroPayOut=new EuropeanCallOption(K);
		double price=euroPayOut.getPayout(gbm);
		assertTrue(price>=0);
	}

}
