package montecarlo;

import java.util.List;

import org.apache.commons.math3.util.Pair;
import org.joda.time.DateTime;

import junit.framework.TestCase;

public class Test_GBMRandomPathGenerator extends TestCase {

	public void testGBMRandomPathGenerator() {
		double rate=0.0001;
		double sigma=0.01;
		double S0=152.35;
		int days=252;
		int batch=1024;
		DateTime startDate=new DateTime();
		DateTime endDate=startDate.plusDays(days);
		RandomVectorGenerator rvg=new GPUNormalRandomVectorGenerator(days,batch);
		StockPath gbm=new GBMRandomPathGenerator(rate,days,sigma,S0,startDate,endDate,rvg);
	}

	public void testGetPrices() {
		double rate=0.0001;
		double sigma=0.01;
		double S0=152.35;
		int days=2;
		int batch=1024;
		DateTime startDate=new DateTime();
		DateTime endDate=startDate.plusDays(days);
		RandomVectorGenerator rvg=new GPUNormalRandomVectorGenerator(days,batch);
		StockPath gbm=new GBMRandomPathGenerator(rate,days,sigma,S0,startDate,endDate,rvg);
		List<Pair<DateTime,Double>> prices=gbm.getPrices();
		assertTrue(prices.size()==days+1);
		assertTrue(prices.get(0).getKey().equals(startDate.plusDays(0)));
		assertTrue(prices.get(1).getKey().equals(startDate.plusDays(1)));
	}

}
