package montecarlo;

import junit.framework.TestCase;
import org.joda.time.DateTime;

public class Test_GPUNormalRandomVectorGenerator extends TestCase {

    //Test for the constructor
    public void testGPUNormalRandomVectorGenerator() {
        //test the constructor
        RandomVectorGenerator rvg=new GPUNormalRandomVectorGenerator(2,1024);
    }

    //Test for the getVector method
    public void testGetVector() {
        //initialize the GPU version normal random number generator
        RandomVectorGenerator rvg=new GPUNormalRandomVectorGenerator(2,1024);
        double[] vector=rvg.getVector();
        //check if the number of random number we get equals to what we ask for
        assertTrue(vector.length==2);
    }

    //Test for the generateOneBatchRandomVector method
    public void testGenerateOneBatchRandomVector() {
        //initialize the batch size
        int batch=512*512*8;
        //define the tolerance range of mean and standard deviation
        double err=0.01;
        //initialize the GPU version normal random number generator
        GPUNormalRandomVectorGenerator rvg1=new GPUNormalRandomVectorGenerator(1,batch);
        //generate a batch of random numbers
        rvg1.generateOneBatchRandomVector();
        //check if the number of random number generated equals to what we ask for
        assertTrue(rvg1.bigNormalRandomVector.length==batch);
        //initialize the statistics calculator
        StatsCaculator statsCaculator=new StatsCaculator();
        for(int i=0;i<batch;i++){
            //get the random number from the random vector one by one
            double randomNumber=rvg1.getVector()[0];
            //calculate the statistics, mean and standard deviation of the random vector along the fly
            statsCaculator.calculateNewStandardDeviation(randomNumber);
        }
        //check if the mean and standard deviation of the random vector is within tolerance range
        assertTrue(statsCaculator.getRunningAverage()<=err);
        assertTrue(Math.abs(statsCaculator.getStandardDeviation()-1)<=err);
    }

}
