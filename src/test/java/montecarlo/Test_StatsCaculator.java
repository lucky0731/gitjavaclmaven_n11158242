package montecarlo;

import junit.framework.TestCase;

public class Test_StatsCaculator extends TestCase {

	public void testStatsCaculator() {
		StatsCaculator statsCalculator=new StatsCaculator();
	}

	public void testCalculateNewStandardDeviation() {
		StatsCaculator statsCalculator=new StatsCaculator();
		assertTrue(statsCalculator.calculateNewStandardDeviation(2)==0);
		assertTrue(statsCalculator.getStandardDeviation()-2<10e-5);
		assertTrue(statsCalculator.getRunningAverage()-2<10e-5);
		assertTrue(statsCalculator.calculateNewStandardDeviation(1)-Math.sqrt(5-1.5*1.5)<10e-5);
		assertTrue(statsCalculator.getStandardDeviation()-Math.sqrt(5)<10e-5);
		assertTrue(statsCalculator.getRunningAverage()-1.5<10e-5);
	}

	public void testGetStandardDeviation() {
		StatsCaculator statsCalculator=new StatsCaculator();
		assertTrue(statsCalculator.getStandardDeviation()<10e-5);
	}

	public void testGetRunningAverage() {
		StatsCaculator statsCalculator=new StatsCaculator();
		assertTrue(statsCalculator.getRunningAverage()<10e-5);
	}

}
